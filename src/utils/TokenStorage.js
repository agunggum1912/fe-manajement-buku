export default {
  get() {
    return localStorage.getItem('userInfo') !== null ? JSON.parse(localStorage.getItem('userInfo')).token : ''
  }
}