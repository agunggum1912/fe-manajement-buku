// axios
import axios from 'axios'
import TokenStorage from './utils/TokenStorage'

// const baseURL = ''
const token = TokenStorage.get()

const headers = {}
if(token) headers.Authorization = `Bearer ${token}`

export default axios.create({
  // baseURL,
  headers
  // You can add your headers here
})
